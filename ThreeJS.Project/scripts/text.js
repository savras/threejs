var text = (function () {
    'use strict';
    
    return {
        createText: createText
    };
    
    function createText(textContent, name) {
        var text3d = new THREE.TextGeometry(textContent, {
                        size: 50,
                        height: 20,
                        curveSegments: 2,
                        font: 'helvetiker'
                     });
        
        var textMaterial = new THREE.MeshBasicMaterial({ color: 0xff0000});

        var textMesh = new THREE.Mesh(text3d, textMaterial);

        textMesh.position.set(-200, -60, -1200);
        textMesh.name = textContent;

        game.scene.add(textMesh);
        game.winText = textMesh;
    }
}());